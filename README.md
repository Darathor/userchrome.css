Ce d�p�t regroupe les styles sp�cifiques que j'applique sur mon Firefox, essentiellement pour palier aux changements d'interface que l'ont ne peut plus compenser via des extensions. 

Ces styles ne sont pour la plupart pas de moi, cf les en-t�tes de chaque fichier pour trouver leurs sources.

## Styles int�gr�s

 * replacement de la barre d'onglets en dessous des autres
 * recyclage des espaces flexibles que l'on peut placer dans les barres d'outils en s�parateurs
 * recyclage de la barre personnelle en barre d'�tat
 
## Installation

1. dans Firefox, ouvrir `about:profiles`
2. dans le profil courant, chercher la ligne "R�pertoire racine" et cliquer sur le bouton "Ouvrir le dossier"
3. cloner ce projet dans un sous-dossier `chrome`
4. red�marrer Firefox